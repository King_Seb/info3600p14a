import Pix2Pix
import tensorflow as tf
import numpy as np

class EyeLabeler(object):

    def __init__(self):

        self.model = Pix2Pix.pix2pix(
            tf.Session(),
            image_size=256,
            batch_size=1,
            output_size=256,
            dataset_name="eye2label",
            checkpoint_dir="checkpoint",
        )

        return

    def label(self, image):

        image = image/255.0


        label = self.model.process(image)
        label.clip(0.0, 1.0)

        output = (label * 255.0).astype(np.uint8).clip(0, 255)

        return output

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import os
    files = os.listdir("test_images/")
    labelizer = EyeLabeler()
    for f in files:
        img = plt.imread("test_images/" + f)[:, :, 0:3]
        label = labelizer.label(img)
        plt.imsave("output_labels/" + f, (img+label)*0.5)
