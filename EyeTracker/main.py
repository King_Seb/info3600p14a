import argparse
from eye_tracker import *

# add argument if you need
parser = argparse.ArgumentParser(description='')
parser.add_argument('--use_local_camera', dest='use_local_camera', default=1, help='if we use local cameras. (0=False, 1=True)')
parser.add_argument('--send_eyesight_vector', dest='send_eyesight_vector', default=0, help='if we need send eyesight vector to others. (0=False, 1=True)')
parser.add_argument('--left_video_source', dest='left_video_source', default=0, help='index of the left camera.')
parser.add_argument('--right_video_source', dest='right_video_source', default=1, help='index of the right camera.')
parser.add_argument('--remote_video_source_url', dest='remote_video_source_url', default="tcp://192.168.93.1:9014", help='url of remote video stream.')
parser.add_argument('--ipv4Address', type=str, dest = 'ipv4_address' , default="10.117.118.199", help = 'the ipv4 address of host in current network environment')

args = parser.parse_args()


def main():
    eye_tracker = EyeTracker(args=args)
    eye_tracker.run()

if __name__ == '__main__':
    main()