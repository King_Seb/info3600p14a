from feature_extractor import *
from eye_labeler import *
import pygame
import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
import socket
import struct
from eye_tracker import *


def resizeImage(image, zoom, targetHeight, targetWidth, offset = None):
    h, w = image.shape[0], image.shape[1]
    ah = h / zoom
    if offset is None: offset = [0, 0]
    img = image[int((h-ah)/2.0) + offset[1]:int((h-ah)/2.0 + ah) + offset[1], int((w-h)/2 + (h-ah)/2) + offset[0]:int((w-h)/2 + (h-ah)/2 + ah) + offset[0], 0:3]
    # height, width, channels = img.shape
    resized_img = cv2.resize(img, (targetWidth, targetHeight))
    return resized_img

def drawImage(pygame_screen, img, position):
    img = np.rot90(img)
    img = pygame.surfarray.make_surface(img)
    pygame_screen.blit(img, position)

def drawText(pygame_screen, text, position, reverse_color = False):
    font = pygame.font.SysFont(None, 36)
    
    if reverse_color:
	text = font.render(text, True, (0, 0, 0))
    else:
	text = font.render(text, True, (255, 255, 255))
    pygame_screen.blit(text, position)

def drawDot(pygame_screen, position, size = 32, color = (255, 255, 255), reverse_color = False):
    # w, h = size,size
    s = pygame.display.get_surface()
    # r = pygame.Rect(position[0] - w//2, position[1] - h//2, w, h)
    # s.fill(color, r)

    if reverse_color:
	color = (255-color[0], 255-color[1], 255-color[2])

    pygame.draw.circle(s, color, (int(position[0]), int(position[1])), int(size/2))

def drawPolygon(pygame_screen, point_list):
    s = pygame.display.get_surface()
    pygame.draw.polygon(s, (255, 0, 0), point_list, 3)

class UserInterface(object):

    def __init__(self, cap, eye_labelizer, feature_extractor, eye_tracker, send_eyesight_vector=False, ipv4Address = '', reverse_color = False):
        # init pygame
        pygame.init()
        infoObject = pygame.display.Info()
        self.width = infoObject.current_w
        self.height = infoObject.current_h
        self.screen = pygame.display.set_mode((self.width, self.height), pygame.FULLSCREEN)
        pygame.display.set_caption('Pygame Keyboard Test')
        pygame.mouse.set_visible(0)

        # self.cap_left = cap_left
        # self.cap_right = cap_right
        self.cap = cap

        self.labelizer = eye_labelizer
        self.feature_extractor = feature_extractor
        self.eye_tracker = eye_tracker
        self.previous_not_none_coordinate = None
        self.previous_not_none_alpha_betas = None
        self.indicater_mode = "bar"
	self.reverse_color = reverse_color

        self.zoom_rate = 2.4
        self.left_offset = [0, 0]
        self.right_offset = [0, 0]
        self.temp_coordinates = []

        self.conn = None
        if send_eyesight_vector:
            self.host = ipv4Address
            self.port = 8333
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.bind((self.host, self.port))
            self.socket.listen(5)
            self.waitForOneConnection()

    def waitForOneConnection(self):
        print("wait for a connection... ")
        conn, address = self.socket.accept()
        self.conn = conn
        # send initial data
        data = b''
        data += struct.pack('f', 0.5)
        data += struct.pack('f', 0.5)
        conn.send(data)
        print("Connected! Socket address is {}".format(address))

    def getLeftRightImage(self):

        left_img, right_img = self.cap.read()
        print(self.zoom_rate,self.right_offset, self.left_offset)
        if not (left_img is None):
            left_img = resizeImage(left_img, self.zoom_rate, 256, 256, offset=self.left_offset)

        if not (right_img is None):
            right_img = resizeImage(right_img, self.zoom_rate, 256, 256, offset=self.right_offset)

        return left_img, right_img

    def update_and_draw_dots(self, alpha_betas):
        if alpha_betas!=None:

            abl, abr = alpha_betas
            a = (abl[0] + abr[0]) * 0.5
            b = (abl[1] + abr[1]) * 0.5
            coordinate = [a * self.width, b*self.height]

            if len(self.temp_coordinates) == 20:
                self.temp_coordinates.pop(0)

            self.temp_coordinates.append(coordinate)
            self.previous_not_none_coordinate = coordinate

        for i in range(0, len(self.temp_coordinates)):
            index = len(self.temp_coordinates) - 1 - i
            drawDot(self.screen, self.temp_coordinates[i], size=int(32 - index * 1.5),
                    color=(255 - index * 10, 255 - index * 10, 255 - index * 10), reverse_color = self.reverse_color)


    def update_indicater(self, difference):
        if difference == None:
            return

        # currently disable the indicator
        return

        if self.indicater_mode == "color":
            difference -= 0.3
            difference = max(0, difference)


            inverse_diff = 1 / (difference + (1 / 255.0))

            G = max(int(255 - difference * 500), 0)
            R = min(int(difference * 500), 255)

            drawDot(self.screen, (self.width / 2, self.height / 2), size=150, color=(R, G, 0))

        elif self.indicater_mode =="bar":
            x1 = int(self.width/5 + 150)
            y1 = int(self.height / 4 * 3 - 100)
            gamma = 20
            B = 0.09
            C = 0.35
            difference = max(difference, C+0.001)
            length = 50*B*(1-C)/(C*(difference-C))
            pygame.draw.rect(self.screen, (255, 255, 255), (x1, y1, length, 10), 0)


    def collectInitialData(self):
        running = True

        coordinates = [
            [0, 0],
            [self.width, 0],
            [self.width, self.height],
            [0, self.height]
        ]

        left_labels = [None, None, None, None]
        right_labels = [None, None, None, None]

        left_features = [None, None, None, None]
        right_features = [None, None, None, None]

        count = -1
        choice = 0
        recorded_dots = []
        log_list = []

        while running:
	    if self.reverse_color:
		self.screen.fill([255, 255, 255])
            else:
		self.screen.fill([0, 0, 0])
            left_img, right_img = self.getLeftRightImage()


            if left_img is not None:
                left_output = left_img

                if choice == 3:
                    left_label = self.labelizer.label(left_img)
                    _, _, _, left_feature = self.feature_extractor.extract(left_label)
                    if left_feature is None:
                        left_output = (left_img * 0.5).astype(np.uint8)
                    else:
                        left_output = (left_img * 0.5 + left_feature * 0.5).astype(np.uint8)

                elif choice == 2:
                    left_label = self.labelizer.label(left_img)
                    left_output = (left_img * 0.5 + left_label * 0.5).astype(np.uint8)

                elif choice == 1:
                    left_label = self.labelizer.label(left_img)
                    left_output = left_label

                drawImage(self.screen, left_output, [0, self.height / 2 - 128])

            if right_img is not None:

                right_output = right_img

                if choice == 3:
                    right_label = self.labelizer.label(right_img)
                    _, _, _, right_feature = self.feature_extractor.extract(right_label)
                    if right_feature is None:
                        right_output = (right_img * 0.5).astype(np.uint8)
                    else:
                        right_output = (right_img * 0.5 + right_feature * 0.5).astype(np.uint8)

                elif choice == 2:
                    right_label = self.labelizer.label(right_img)
                    right_output = (right_img * 0.5 + right_label * 0.5).astype(np.uint8)

                elif choice == 1:
                    right_label = self.labelizer.label(right_img)
                    right_output = right_label

                # right_output = resizeImage(right_output, 1.0, 512, 512)
                drawImage(self.screen, right_output, [self.width - 256, self.height / 2 - 128])


            if count == -1:
                drawText(self.screen, "When you are ready,", ((int(self.width / 3), int(self.height / 5 * 1))), reverse_color = self.reverse_color)
                drawText(self.screen, "please press E", ((int(self.width / 3), int(self.height / 5 * 2))), reverse_color = self.reverse_color)


            elif count>=0:
                drawText(self.screen, "Please stare at white", ((int(self.width / 3), int(self.height / 5 * 1))), reverse_color = self.reverse_color)
                drawText(self.screen, "point and press E", ((int(self.width / 3), int(self.height / 5 * 2))), reverse_color = self.reverse_color)
                drawText(self.screen, str(log_list), ((int(self.width / 3), int(self.height / 5 * 3))), reverse_color = self.reverse_color)

                # draw dot
                drawDot(self.screen, coordinates[count], reverse_color = self.reverse_color)
                for dot in recorded_dots:
                    drawDot(self.screen, dot, 5, (255,0,255), reverse_color = self.reverse_color)

            pygame.display.update()

            for event in pygame.event.get():
                if (event.type == pygame.KEYUP):
                    if (event.key == pygame.K_q):
                        running = False
                    if (event.key == pygame.K_l):
                        choice = (choice + 1) % 4
                        print("choice =", choice)
                    if (event.key == pygame.K_1):
                        self.zoom_rate = max(1.4, self.zoom_rate - 0.1)
                    if (event.key == pygame.K_2):
                        self.zoom_rate = min(3.8, self.zoom_rate + 0.1)

                    if (event.key == pygame.K_DOWN):
                        self.right_offset[1] = max(-100, self.right_offset[1] - 5)
                    if (event.key == pygame.K_UP):
                        self.right_offset[1] = min(100, self.right_offset[1] + 5)
                    if (event.key == pygame.K_LEFT):
                        self.right_offset[0] = max(-100, self.right_offset[0] - 5)
                    if (event.key == pygame.K_RIGHT):
                        self.right_offset[0] = min(100, self.right_offset[0] + 5)
                    if (event.key == pygame.K_s):
                        self.left_offset[1] = max(-100, self.left_offset[1] - 5)
                    if (event.key == pygame.K_w):
                        self.left_offset[1] = min(100, self.left_offset[1] + 5)
                    if (event.key == pygame.K_a):
                        self.left_offset[0] = max(-100, self.left_offset[0] - 5)
                    if (event.key == pygame.K_d):
                        self.left_offset[0] = min(100, self.left_offset[0] + 5)

                    if (event.key == pygame.K_e):
                        if count >= 0:

                            left_label = self.labelizer.label(left_img)
                            right_label = self.labelizer.label(right_img)
                            left_feature = self.feature_extractor.extract(left_label)
                            right_feature = self.feature_extractor.extract(right_label)


                            if (left_feature[0] != None and right_feature[0] != None):

                                #plt.imsave("output/left-img-{}.png".format(count), left_img)
                                #plt.imsave("output/right-img-{}.png".format(count), right_img)
                                #plt.imsave("output/left-label-{}.png".format(count), left_label)
                                #plt.imsave("output/right-label-{}.png".format(count), right_label)
                                #plt.imsave("output/left-feature-{}.png".format(count), left_feature[3])
                                #plt.imsave("output/right-feature-{}.png".format(count), right_feature[3])

                                left_labels[count] = left_label
                                right_labels[count] = right_label
                                left_features[count] = left_feature
                                right_features[count] = right_feature

                                left_feature[0][0] = 255 - left_feature[0][0]
                                right_feature[0][0] = 255 - right_feature[0][0]



                                log_list.append([left_feature[0], right_feature[0]])

                                recorded_dots.append([left_feature[0][0], left_feature[0][1] + self.height / 2 - 128])
                                recorded_dots.append([self.width - 256 + right_feature[0][0], right_feature[0][1] + self.height / 2 - 128])

                                count += 1

                        elif count == -1:
                            count += 1

                        if (count == 4):
                            running = False

        self.left_init_pupils = [feature[0] for feature in left_features]
        self.right_init_pupils = [feature[0] for feature in right_features]

        #
        # left_labels = [self.labelizer.labelize(image) for image in left_eye_images]ee
        # right_labels = [self.labelizer.labelize(image) for image in right_eye_images]
        #
        # left_features = [self.feature_extractor.extract((label*255).astype(np.uint8)) for label in left_labels]
        # right_features = [self.feature_extractor.extract((label*255).astype(np.uint8)) for label in right_labels]

        return left_features, right_features

    def update(self, result, left_image, right_image, left_label, right_label, log_list = None):
	if self.reverse_color:
	    self.screen.fill([255, 255, 255])
	else:
            self.screen.fill([0, 0, 0])

        left_output = None

        alpha_betas, alpha_difference = result

        if alpha_betas is not None:
            self.previous_not_none_alpha_betas = alpha_betas



        if (self.conn is not None) and (self.previous_not_none_alpha_betas is not None):
            alpha_avg = (self.previous_not_none_alpha_betas[0][0] + self.previous_not_none_alpha_betas[1][0]) / 2
            beta_avg = (self.previous_not_none_alpha_betas[0][1] + self.previous_not_none_alpha_betas[1][1]) / 2
            data = b''
            data += struct.pack('f', alpha_avg)
            data += struct.pack('f', beta_avg)
            self.conn.send(data)

        if result is not None:
            center, alpha_difference = result
            self.update_indicater(alpha_difference)

        if not (left_label is None):
            left_output = (left_image * 0.5 + left_label * 0.5).astype(np.uint8)
        elif not (left_image is None):
            left_output = left_image

        if not (left_output is None):
            drawImage(self.screen, left_output, [0, self.height / 2 - 128])

        right_output = None

        if not (right_label is None):
            right_output = (right_image * 0.5 + right_label * 0.5).astype(np.uint8)
        elif not (right_image is None):
            right_output = right_image

        if not (right_output is None):
            drawImage(self.screen, right_output, [self.width - 256, self.height / 2 - 128])


        if not (log_list is None):
            i = 1
            for text in log_list:
                drawText(self.screen, text, [0, 100 + 100 * i], reverse_color = self.reverse_color)
                i += 1

        drawPolygon(self.screen, [
            (self.left_init_pupils[0][0], self.left_init_pupils[0][1] + self.height / 2 - 128),
            (self.left_init_pupils[1][0], self.left_init_pupils[1][1] + self.height / 2 - 128),
            (self.left_init_pupils[2][0], self.left_init_pupils[2][1] + self.height / 2 - 128),
            (self.left_init_pupils[3][0], self.left_init_pupils[3][1] + self.height / 2 - 128)
        ])
        drawPolygon(self.screen, [
            (self.right_init_pupils[0][0] + self.width - 256, self.right_init_pupils[0][1] + self.height / 2 - 128),
            (self.right_init_pupils[1][0] + self.width - 256, self.right_init_pupils[1][1] + self.height / 2 - 128),
            (self.right_init_pupils[2][0] + self.width - 256, self.right_init_pupils[2][1] + self.height / 2 - 128),
            (self.right_init_pupils[3][0] + self.width - 256, self.right_init_pupils[3][1] + self.height / 2 - 128)
        ])

        self.update_and_draw_dots(alpha_betas)


        for event in pygame.event.get():
            if (event.type == pygame.KEYUP):
                if (event.key == pygame.K_q):
                    exit()

                if (event.key == pygame.K_c):
                    self.temp_coordinates.clear()

                if (event.key == pygame.K_r):
                    self.eye_tracker.calibrate()
                    self.eye_tracker.run()

                if (event.key == pygame.K_1):
                    self.indicater_mode = "bar"

                if (event.key == pygame.K_2):
                    self.indicater_mode = "color"
        pygame.display.update()