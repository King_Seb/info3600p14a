import cv2
import numpy as np
import math

class FeatureExtractor(object):

    def __init__(self, corner_threshold = 5, shift = 3):
        self.eye_corner_threshold = corner_threshold
        self.eye_corner_shift = shift

    # extract pupil center and two eye corners
    def extract(self, label_image, extractPupil=True, extractCorner=False):

        if extractCorner and extractPupil:
            center = self.findPupilCenter(label_image)
            left_corner = self.findLeftEyeCorner(label_image)
            right_corner = self.findRightEyeCorner(label_image)
            return ((int(center[0]), int(center[1])),
                    (int(left_corner[0]), int(left_corner[1])),
                    (int(right_corner[0]), int(right_corner[1])))

        if extractPupil:
            center = self.findPupilCenter(label_image)
            return (int(center[0]), int(center[1]))

        if extractCorner:
            left_corner = self.findLeftEyeCorner(label_image)
            right_corner = self.findRightEyeCorner(label_image)
            return  ((int(left_corner[0]), int(left_corner[1])), (int(right_corner[0]), int(right_corner[1])))

    def findPupilCenter(self, label_image):
        totalRow = 0
        totalCol = 0
        count = 0
        for i in range(label_image.shape[0]):
            for j in range(label_image.shape[1]):
                if label_image[i,j,2] >= 240:
                    totalCol += i
                    totalRow += j
                    count += 1
        if count == 0:
            print("Fail to detect pupil center")
            return None
        return (totalRow/count, totalCol/count)


    def findLeftEyeCorner(self, label_image):
        count = 0
        col = 0
        row = 0
        for i in range(label_image.shape[0]):
            for j in range(label_image.shape[1]):
                if label_image[j,i,1] >= self.eye_corner_threshold:
                    count += 1
                else:
                    count = 0
                if count >= self.eye_corner_threshold:
                    count = 0
                    col = i - self.eye_corner_shift
                    if col < 0:
                        col = 0
                    for j in range(label_image.shape[1]):
                        if label_image[j,i,1] >= self.eye_corner_threshold:
                            row += j
                            count += 1
                    row /= count
                    return (col, row)
        print("Fail to detect left eye corner")
        return None


    def findRightEyeCorner(self, label_image):
        count = 0
        col = 0
        row = 0
        for i in range(label_image.shape[0]-1, -1, -1):
            for j in range(label_image.shape[1]):
                if label_image[j,i,1] >= self.eye_corner_threshold:
                    count += 1
                else:
                    count = 0
                if count >= self.eye_corner_threshold:
                    count = 0
                    col = i + self.eye_corner_shift
                    if col >= label_image.shape[0]:
                        col = label_image.shape[0] -1
                    for j in range(label_image.shape[1]):
                        if label_image[j,i,1] >= self.eye_corner_threshold:
                            row += j
                            count += 1
                    row /= count
                    return (col, row)
        print("Fail to detect right eye corner")
        return None

    # convert the feature points into machine learning model input vector
    def getFeatureVector(self, label_image):
        center = self.findPupilCenter(label_image)
        left_corner = self.findLeftEyeCorner(label_image)
        right_corner = self.findRightEyeCorner(label_image)
        left_to_center_distance = (center[0]-left_corner[0]) * (center[0]-left_corner[0]) + (center[1]-left_corner[1]) * (center[1]-left_corner[1])
        left_to_center_distance = math.sqrt(left_to_center_distance)
        right_to_center_distance = (center[0]-right_corner[0]) * (center[0]-right_corner[0]) + (center[1]-right_corner[1]) * (center[1]-right_corner[1])
        right_to_center_distance = math.sqrt(right_to_center_distance)
        left_to_center_x = abs(center[0]-left_corner[0])
        right_to_center_x = abs(right_corner[0]-center[0])
        degree_left = 0.0
        degree_right = 0.0
        if left_to_center_distance != 0:
            degree_left = math.acos(left_to_center_x / left_to_center_distance)
        if right_to_center_distance != 0:
            degree_right = math.acos(right_to_center_x / right_to_center_distance)
        if center[1] < left_corner[1]:
            degree_left = - degree_left
        if center[1] < right_corner[1]:
            degree_right = - degree_right
        return np.array([center[0],
                        center[1],
                        float(left_corner[0]),
                        float(left_corner[1]),
                        float(right_corner[0]),
                        float(right_corner[1]),
                        left_to_center_distance,
                        right_to_center_distance,
                        degree_left,
                        degree_right])





