import cv2

class VideoStreamReceiver(object):

    def __init__(self, address, use_local,  left_index = None, right_index = None):
        self.address = address
        if use_local:
            assert left_index is not None
            assert right_index is not None
            self.cap_left = cv2.VideoCapture(left_index)
            self.cap_right= cv2.VideoCapture(right_index)
        else:
            self.cap = cv2.VideoCapture(address)
        self.use_local = use_local
        self.video_width = 640
        self.video_height = 426

    def read(self):

        if self.use_local:
            ret, left_frame = self.cap_left.read()
            ret, right_frame = self.cap_right.read()
            if (left_frame is None) or (right_frame is None):
                return None, None
            return left_frame, right_frame
        else:
            ret, frame = self.cap.read()
            last_frame = None
            while frame is not None:
                last_frame = frame
                ret, frame = self.cap.read()
            frame = last_frame

            numVideos = 2

            if frame is None:
                return None, None

            images = [None] * numVideos
            for i in range(numVideos):
                images[i] = frame[i * self.video_height:(i + 1) * self.video_height, 0:self.video_width, 0:3]

            left_frame, right_frame = images
            return left_frame, right_frame