import numpy as np

def dot(v, u):
    return v[0]*u[0] + v[1]*u[1]

def findAlpha(A, B, C, D, P, threshold = 1e-6):

    AB = B - A
    DC = C - D

    # PA = A - P
    # PD = D - P

    # AD_side_direction = PA + PD
    AD_side_direction_sign = ((A[1] - D[1]) * P[0] + (D[0] - A[0]) * P[1] + A[0] * D[1] - A[1] * D[0]) > 0

    low = -1
    high = 2
    while high - low > threshold:
        mid = (low + high) * 0.5
        E = mid * AB + A
        F = mid * DC + D
        # PF = F - P
        # PE = E - P
        temp = ((E[1] - F[1]) * P[0] + (F[0] - E[0]) * P[1] + E[0] * F[1] - E[1] * F[0])

        if (temp == 0):
            return mid

        elif ((temp>0) == AD_side_direction_sign):
            # mid is small
            low = mid

        else:
            high = mid

    alpha = (low + high) * 0.5

    return alpha

def findBeta(A, B, C, D, P, threshold=1e-6):
    AD = D - A
    BC = C - B

    # PA = A - P
    # PB = B - P

    # AB_side_direction = PA + PB
    AB_side_direction_sign = ((A[1] - B[1]) * P[0] + (B[0] - A[0]) * P[1] + A[0] * B[1] - A[1] * B[0]) > 0
    # print(AB_side_direction_sign)

    low = -1
    high = 2
    while high - low > threshold:
        mid = (low + high) * 0.5
        G = mid * AD + A
        H = mid * BC + B
        # PG = G - P
        # PH = H - P
        temp = ((G[1] - H[1]) * P[0] + (H[0] - G[0]) * P[1] + G[0] * H[1] - G[1] * H[0])
        if (temp == 0):
            return mid

        elif ((temp > 0) == AB_side_direction_sign):
            # mid is small
            low = mid

        else:
            high = mid

    beta = (low + high) * 0.5

    return beta

class GazeEstimator(object):


    # left_feature = left pupil
    # right_feature = right pupil

    def __init__(self, init_left_features, init_right_features):
        self.init_left_pupil_pos = init_left_features
        self.init_right_pupil_pos = init_right_features
        # print(init_coordinates)
        # print(init_left_features)
        # print(init_right_features)

    def predict(self, left_features, right_features, log = False):
        # print("lf:", left_features)
        # print("rf:", right_features)
        # https://math.stackexchange.com/questions/1680520/how-to-find-the-original-coordinates-of-a-point-inside-an-irregular-rectangle
        # https://cs.stackexchange.com/questions/53909/how-to-find-the-original-coordinates-of-a-point-inside-an-irregular-rectangle

        # left, right pupil position
        abl, abr = None, None

        print("left", self.init_left_pupil_pos[0][0],
              self.init_left_pupil_pos[1][0],
              self.init_left_pupil_pos[2][0],
              self.init_left_pupil_pos[3][0])
        print("right", self.init_right_pupil_pos[0][0],
              self.init_right_pupil_pos[1][0],
              self.init_right_pupil_pos[2][0],
              self.init_right_pupil_pos[3][0])


        print(left_features[0], right_features[0])

        if len(np.array(left_features).shape) > 0:
            if (left_features[0]) != None:
                left_features[0][0] = 255 - left_features[0][0]
                abl = self.calculateAlphaBeta(
                    [self.init_left_pupil_pos[0][0][0], self.init_left_pupil_pos[0][0][1]],
                    [self.init_left_pupil_pos[1][0][0], self.init_left_pupil_pos[1][0][1]],
                    [self.init_left_pupil_pos[2][0][0], self.init_left_pupil_pos[2][0][1]],
                    [self.init_left_pupil_pos[3][0][0], self.init_left_pupil_pos[3][0][1]],
                    [left_features[0][0], left_features[0][1]]
                )

        if len(np.array(right_features).shape) > 0:
            if (right_features[0]) != None:
                right_features[0][0] = 255 - right_features[0][0]
                abr = self.calculateAlphaBeta(
                    [self.init_right_pupil_pos[0][0][0], self.init_right_pupil_pos[0][0][1]],
                    [self.init_right_pupil_pos[1][0][0], self.init_right_pupil_pos[1][0][1]],
                    [self.init_right_pupil_pos[2][0][0], self.init_right_pupil_pos[2][0][1]],
                    [self.init_right_pupil_pos[3][0][0], self.init_right_pupil_pos[3][0][1]],
                    [right_features[0][0], right_features[0][1]]
                )

        result = None
        alpha_difference = None
        alpha_betas = None

        if abl!=None and abr!=None:
            alpha_difference = abs(abl[0] - abr[0])
            abl[0] = min(max(abl[0], 0), 1)
            abl[1] = min(max(abl[1], 0), 1)
            abr[0] = min(max(abr[0], 0), 1)
            abr[1] = min(max(abr[1], 0), 1)
            alpha_betas = [abl,abr]
            result = [alpha_betas, alpha_difference]
        else:
            result = [None, None]

        if log:
            log_list = [
                "Left Pupil: " + str(left_features[0]),
                "Right Pupil: " + str(right_features[1]),
                "Left AB: " + str(abl),
                "Right AB: " + str(abr)
            ]
            if alpha_difference is not None:
                log_list = log_list + [
                    "AB diff: " + str(alpha_difference+0.001),
                    "1/ AB diff: " + str(1/(alpha_difference+1/255.0))
                ]
            result = result + [log_list]

        return result

    def calculateAlphaBeta(self, lupp, rupp, rdpp, ldpp, pp):

        A = np.array(lupp)
        B = np.array(rupp)
        C = np.array(rdpp)
        D = np.array(ldpp)
        P = np.array(pp)

        alpha = findAlpha(A, B, C, D, P)
        beta = findBeta(A, B, C, D, P)

        return [alpha, beta]



        # 4 init coordinates
        # lx = self.init_coordinates[0][0]
        # rx = self.init_coordinates[1][0]
        # uy = self.init_coordinates[1][1]
        # dy = self.init_coordinates[2][1]
        #
        #
        #
        # ppx, ppy = pp[0], pp[1]
        #
        # luppx, luppy = lupp[0], lupp[1]
        # ruppx, ruppy = rupp[0], rupp[1]
        # rdppx, rdppy = rdpp[0], rdpp[1]
        # ldppx, ldppy = ldpp[0], ldpp[1]
        #
        # al1 = ppy - luppy
        # ar1 = ppy - ruppy
        #
        # au2 = ruppx - ppx
        # ad2 = rdppx - ppx
        #
        # al3 = ldppy - ppy
        # ar3 = rdppy - ppy
        #
        # au4 = ppx - luppx
        # ad4 = ppx - ldppx
        #
        #
        #
        # # screen width & height
        #
        #
        # # eye width & height
        # # sw = abs(ru_pp[0] - lu_pp[0])
        # # sh = abs(rd[1] - ru[1])
