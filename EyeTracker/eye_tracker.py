import cv2
from feature_extractor import *
from eye_labeler import *
from user_interface import *
from gaze_estimator import *
from feature_extractor2 import *
from video_stream_receiver import *

class EyeTracker(object):

    def __init__(self, args):


        # init left cameras, set both width and hright to 256
        if (args.use_local_camera==0):
            self.cap = VideoStreamReceiver(args.remote_video_source_url)
        else:
            self.cap = VideoStreamReceiver(None, True, args.left_video_source, args.right_video_source)

        # init eye_labelizer and feature_extractor
        self.eye_labelizer = EyeLabeler()
        self.feature_extractor = FeatureExtractor2()

        # collect initial user data (GUI)
        self.collector = UserInterface(
            self.cap,
            eye_labelizer=self.eye_labelizer,
            feature_extractor=self.feature_extractor,
            eye_tracker = self,
            send_eyesight_vector=(args.send_eyesight_vector==1),
            ipv4Address = args.ipv4_address
        )

        self.is_running = False
        self.calibrate()

    def calibrate(self):
        self.is_running = False
        init_left_features, init_right_features = self.collector.collectInitialData()

        # init coordinate_predictor
        self.coordinate_predictor = GazeEstimator(init_left_features, init_right_features)

    def run(self):
        self.is_running = True
        while self.is_running:
            left_image, right_image = self.collector.getLeftRightImage()

            # get labels
            left_features = [None, None, None, None]
            if not (left_image is None):
                left_label = self.eye_labelizer.label(left_image)
                left_features = self.feature_extractor.extract(left_label)

            right_features = [None, None, None, None]
            if not (right_image is None):
                right_label = self.eye_labelizer.label(right_image)
                right_features = self.feature_extractor.extract(right_label)

            alpha_betas, alpha_difference, log_list = self.coordinate_predictor.predict(left_features, right_features, log=True)

            # print(coordinate)
            self.collector.update([alpha_betas, alpha_difference], left_image, right_image, left_features[3], right_features[3],
                                  log_list=log_list)
