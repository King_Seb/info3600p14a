# Project P14A - Eye Tracking Optimiser #

## Project Description
To prioritize video and share experience we need to know where people are looking. The current systems may solve the problem but many are weak and bulky. You will investigate use of current eye tracking systems and feed in their data to Project Team 1, to provide a real time resolution optimization and Project Team 4, to prioritize what we send over the network.

## Project Requirements
We put the specifications of all the dependencies of this project into requirements.txt.
To install all the requirements, type the following command bellow into your terminal

```$ pip3 install -r requirements.txt```

## Project Environment

The project is being developed in Python3 and you should install pipenv to manage dependencies for the software.
To install pipenv, type the following command bellow into your terminal

```$ pip3 install pipenv```

Activate the project virtual environment with

```$ pipenv shell```

If the project folder does not have a virtual environment setup, navigate to the *info3600p14a* folder and initialise the environment on your computer with

```$ pipenv --three```

Then just activate the environment.

Exit the virtual environment by typing ```exit```

## System Architecture

![System Architecture](https://bitbucket.org/King_Seb/info3600p14a/downloads/system_architecture.png)

As illustrated in the diagram, the system consists of 6 main components.

### Helmet with Cameras
As the video capeturer in our project, the helmet is installed with a camera mount, 2 IR-sensitive cameras and 2 IR light filters. 

### Raw Image Resizer
In order to maximize the accuracy, the program needs to crop the part contains eye from the raw image and resize it to 256 * 256.

### Eye Labeler
Eye Labeler performs a segmentation to the resized 256 * 256 image. The output label should only contain 3 colors, red, green and blue, which indicate the certain classes.

### Feature Extractor
During this phase, programs filter out some noise points from the eye label and respectively locate the centers of pupil and eye corners, after which it passes results to the Gaze Estimator.

### Calibrator
The program asks user to look at 4 extreme directions so that the calibrator can collect the bounding positions of user's pupil. The gained data is used to estimate the eyesight bounding.

### Gaze Estimator
With the eyesight bouding and the input pupil center position, Gaze Estimator computes the vector of eyesight direction with the power of linear algebra.