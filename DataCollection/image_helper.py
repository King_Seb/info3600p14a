import cv2
import os

def resize(img, targetHeight, targetWidth):
    height, width, channels = img.shape
    resized_img = cv2.resize(img, None, fx=targetWidth/float(width), fy=targetHeight/float(height))
    return resized_img

def crop(img, x, y, w, h):
    cropped_img = img[y:y + h, x:x + w]
    return cropped_img

def createFolder(name):
    if not os.path.exists(name):
        os.makedirs(name)

def rename(oldImagePath, newImagPath):
    os.renames(oldImagePath, newImagPath)

def multiRename(oldImagePaths, newImagPaths):
    for i in range(len(oldImagePaths)):
        rename(oldImagePaths[i], newImagPaths[i])
