import os
import numpy as np
import matplotlib.pyplot as plt


# put all original images in image_folder
# put all eye label images in eye_label_folder
# put all pupil label folder images in image_folder
# the corresponding images and labels should have the same file name like "2.png"

image_folder = "example/image/"
eye_label_folder = "example/eye/"
pupil_label_folder = "example/pupil/"
merged_label_folder = "example/label/"

if not os.path.exists(merged_label_folder):
    os.mkdir(merged_label_folder)

def isSameColor(c1, c2):
    return c1[0] == c2[0] and c1[1] == c2[1] and c1[2] == c2[2]

def aveColor(c1, c2):
    # smooth
    return [(c1[0]+c2[0])*0.5, (c1[1]+c2[1])*0.5, (c1[2]+c2[2])*0.5]

def mergeImage(img_o, img_e, img_p, output_name):
    img = np.ones_like(img_o)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if isSameColor(img_e[i][j], img_o[i][j]):
                img[i][j] = [0, 0, 1.0]
            elif isSameColor(img_p[i][j], img_o[i][j]):
                img[i][j] = aveColor(img_e[i][j], [0, 1.0, 0])
            else:
                img[i][j] = aveColor(img_p[i][j], [1.0, 0, 0])
    plt.imsave(output_name, img)

def main():
    filenames = [f for f in os.listdir(image_folder)]
    print(filenames)
    for f in filenames:
        try:
            print("Processing file {} ...".format(f))
            img_o = plt.imread(image_folder + f)[:, :, 0:3]
            img_e = plt.imread(eye_label_folder + f)[:, :, 0:3]
            img_p = plt.imread(pupil_label_folder + f)[:, :, 0:3]
            mergeImage(img_o, img_e, img_p, merged_label_folder + f)
            print("    Succeeded!")
        except OSError:
            print("    {} is not an image file!".format(f))


if __name__ == "__main__":
    main()
