import numpy as np
import matplotlib.pyplot as plt

import os
import cv2
import random


# run this script after runnig merge_label.py
image_folder = "bad_image/"
output_folder = "bad_data/"

if not os.path.exists(output_folder):
    os.mkdir(output_folder)

def resize(img, targetHeight, targetWidth):
    height, width, channels = img.shape
    resized_img = cv2.resize(img, None, fx=targetWidth/float(width), fy=targetHeight/float(height))
    return resized_img

def crop_img(img, x, y, w, h):
    return img[y:y+h, x:x+w, :]

def blue_img_like(img):
    blue_img = np.zeros_like(img)
    for y in range(blue_img.shape[0]):
        for x in range(blue_img.shape[1]):
            blue_img[y][x] = [0, 0, 1.0]
    return blue_img

def main():
    filenames = [f for f in os.listdir(image_folder)]
    index = 0
    for f in filenames:
        print(index//8)
        try:
            for i in range(0,4):
                index += 1
                image = plt.imread(image_folder + f)[:, :, 0:3]
                size = random.randint(256, min(image.shape[0], image.shape[1]))
                x = random.randint(0, image.shape[1] - size)
                y = random.randint(0, image.shape[0] - size)
                resized_img = resize(crop_img(image, x, y, size, size), 256, 256)
                blue_label = blue_img_like(resized_img)
                output = np.concatenate([blue_label, resized_img], 1)
                plt.imsave(output_folder + "bad-{}.png".format(index), output)
        except OSError:
            print(f, "is not a valid image file.")

if __name__ == '__main__':
    main()
