import cv2
import os

frameFolder = "./square_frames/"
if (not os.path.exists(frameFolder)):
    os.mkdir(frameFolder)

count = 0

vidcap = cv2.VideoCapture('./eye.mp4')
success,image = vidcap.read()


def crop_center(img,cropx,cropy):
    y, x = img.shape[0], img.shape[1]
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return img[starty:starty+cropy,startx:startx+cropx, :]


while success:
    success,image = vidcap.read()
    image = image[:, :, 0:3]
    size = min(image.shape[0], image.shape[1])
    count += 1
    cropped_image = crop_center(image, size, size)
    cv2.imwrite(frameFolder + "%d.png" % count, cropped_image)
    if (count % 100==0): print(count)

print(count)