import numpy as np
import matplotlib.pyplot as plt

import os
import cv2
import random


# run this script after runnig merge_label.py
image_folder = "image/"
label_folder = "label/"
output_folder = "data/"

if not os.path.exists(output_folder):
    os.mkdir(output_folder)

def resize(img, targetHeight, targetWidth):
    height, width, channels = img.shape
    resized_img = cv2.resize(img, None, fx=targetWidth/float(width), fy=targetHeight/float(height))
    return resized_img


def crop_img(img, is_cropped_area, width, height, put_in_center=False):
    left_x, right_x = -1, -1
    up_y, down_y = -1, -1

    for x in range(img.shape[1]):

        flag = False

        for y in range(img.shape[0]):
            if is_cropped_area(img[y][x]):
                flag = True
                break

        if flag and left_x == -1:
            left_x = x
            continue

        if left_x != -1 and (not flag) and right_x == -1:
            right_x = x - 1
            continue

        if (right_x != -1): break

    for y in range(img.shape[0]):
        flag = False

        for x in range(img.shape[1]):
            if is_cropped_area(img[y][x]):
                flag = True
                break

        if flag and (up_y == -1):
            up_y = y
            continue

        if up_y != -1 and (not flag) and down_y == -1:
            down_y = y - 1
            continue

        if (down_y != -1): break

    crop_w = right_x - left_x + 1
    crop_h = down_y - up_y + 1

    max_leftpad = min(left_x, width - crop_w)
    left_pad = random.randint(5, max_leftpad - 5) if max_leftpad>10 else left_x
    right_pad = width - crop_w - left_pad

    max_uppad = min(up_y, height - crop_h)
    up_pad = random.randint(5, max_uppad - 5) if max_uppad>10 else up_y
    down_pad = height - crop_h - up_pad

    left_x -= left_pad
    right_x += right_pad
    up_y -= up_pad
    down_y += down_pad

    return img[up_y: down_y + 1, left_x: right_x + 1, :], left_x, right_x, up_y, down_y

def is_cropped_area(color):
    return color[1] >= 0.8

def adjustBrightness(image, delta):
    if (delta>0):
        increase = delta
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        v = image[:, :, 2]
        v = np.where(v <= 1.0 - increase, v + increase, 1.0)
        image[:, :, 2] = v
        image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
        return image
    else:
        decrease = -delta
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        v = image[:, :, 2]
        v = np.where(v >= decrease, v - decrease, 0.0)
        image[:, :, 2] = v
        image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
        return image

def main():
    filenames = [f for f in os.listdir(image_folder)]
    index = 0
    for f in filenames:
        print(index//8)
        try:
            for i in range(0,8):
                index += 1
                image = plt.imread(image_folder + f)[:, :, 0:3]
                label = plt.imread(label_folder + f)[:, :, 0:3]
                size = random.randint(300, min(image.shape[0], image.shape[1]))
                cropped_label, left_x, right_x, up_y, down_y = crop_img(label, is_cropped_area, size, size)
                cropped_image = image[up_y: down_y + 1, left_x: right_x + 1, :]
                resized_label = resize(cropped_label, 256, 256)
                resized_image = resize(cropped_image, 256, 256)
                k = 0
                if i>0: k = random.uniform(-0.25, 0.25)
                adjusted_image = adjustBrightness(resized_image, k)
                output = np.concatenate([resized_label, adjusted_image], 1)
                plt.imsave(output_folder + "{}.png".format(index), output)
        except OSError:
            print(f, "is not a valid image file.")

if __name__ == '__main__':
    main()
