import random
from image_helper import *

# n_images = 2913
# source_folder =  "./training_frames/"
# target_folder =  "./training_frames/"
# indices = random.sample(range(n_images), n_images//2)
#
# oldnames = [source_folder + "{}.png".format(i) for i in indices]
# newnames = [target_folder + "{}.png".format(i) for i in indices]
#
# multiRename(oldnames, newnames)

source_folder =  "./training_frames/"
num_files = len([f for f in os.listdir(source_folder)])
num_files_per_folder = num_files // 16

for i in range(15):

    files = [f for f in os.listdir(source_folder)]
    target_folder =  "./training_frames_{}/".format(i)
    createFolder(target_folder)
    indices = random.sample(range(len(files)), num_files_per_folder)

    oldnames = [source_folder + files[i] for i in indices]
    newnames = [target_folder + files[i] for i in indices]
    multiRename(oldnames, newnames)