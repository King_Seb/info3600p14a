from feature_extractor2 import *
import numpy as np
import cv2
import os
import math

def calculate_diff(point1, point2):
	return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

if __name__ == '__main__':
	real_folder_path = '/Users/zm/Desktop/eval_real/'
	fake_folder_path = '/Users/zm/Desktop/eval_fake/'
	real_images = sorted(os.listdir(real_folder_path))
	fake_images = sorted(os.listdir(fake_folder_path))

	center_total_error = 0.0
	left_total_error = 0.0
	right_total_error = 0.0

	center_count = 0
	left_count = 0
	right_count = 0

	feature_extractor = FeatureExtractor2()

	for i in range(len(real_images)):
		real_image = cv2.imread(real_folder_path+real_images[i])
		fake_image = cv2.imread(fake_folder_path+fake_images[i])
		real_features = feature_extractor.extract(real_image)
		fake_features = feature_extractor.extract(fake_image)
		if real_features[0] != None and fake_features[0] != None:
			center_total_error += calculate_diff(real_features[0], fake_features[0])
			center_count += 1
		if real_features[1] != None and fake_features[1] != None:
			left_total_error += calculate_diff(real_features[1], fake_features[1])
			left_count += 1
		if real_features[2] != None and fake_features[2] != None:
			right_total_error += calculate_diff(real_features[2], fake_features[2])
			right_count += 1

	print("Pupil Center average error: " + str(center_total_error/center_count))
	print("Left Eye Corner average error: " + str(left_total_error/left_count))
	print("Right Eye Corner average error: " + str(right_total_error/right_count))

