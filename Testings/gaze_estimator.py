import numpy as np

def dot(v, u):
    return v[0]*u[0] + v[1]*u[1]

def findAlpha(A, B, C, D, P, threshold = 1e-6):

    AB = B - A
    DC = C - D

    # PA = A - P
    # PD = D - P

    # AD_side_direction = PA + PD
    AD_side_direction_sign = ((A[1] - D[1]) * P[0] + (D[0] - A[0]) * P[1] + A[0] * D[1] - A[1] * D[0]) > 0

    low = -1
    high = 2
    while high - low > threshold:
        mid = (low + high) * 0.5
        E = mid * AB + A
        F = mid * DC + D
        # PF = F - P
        # PE = E - P
        temp = ((E[1] - F[1]) * P[0] + (F[0] - E[0]) * P[1] + E[0] * F[1] - E[1] * F[0])

        if (temp == 0):
            return mid

        elif ((temp>0) == AD_side_direction_sign):
            # mid is small
            low = mid

        else:
            high = mid

    alpha = (low + high) * 0.5

    return alpha

def findBeta(A, B, C, D, P, threshold=1e-6):
    AD = D - A
    BC = C - B

    # PA = A - P
    # PB = B - P

    # AB_side_direction = PA + PB
    AB_side_direction_sign = ((A[1] - B[1]) * P[0] + (B[0] - A[0]) * P[1] + A[0] * B[1] - A[1] * B[0]) > 0
    # print(AB_side_direction_sign)

    low = -1
    high = 2
    while high - low > threshold:
        mid = (low + high) * 0.5
        G = mid * AD + A
        H = mid * BC + B
        # PG = G - P
        # PH = H - P
        temp = ((G[1] - H[1]) * P[0] + (H[0] - G[0]) * P[1] + G[0] * H[1] - G[1] * H[0])
        if (temp == 0):
            return mid

        elif ((temp > 0) == AB_side_direction_sign):
            # mid is small
            low = mid

        else:
            high = mid

    beta = (low + high) * 0.5

    return beta


if __name__ == '__main__':

    assert abs(findAlpha(
        np.array([0, 5]),
        np.array([5, 5]),
        np.array([5, 0]),
        np.array([0, 0]),
        np.array([1, 1])
    ) - 0.2) < 0.001
    print(findBeta(
        np.array([0, 5]),
        np.array([5, 5]),
        np.array([5, 0]),
        np.array([0, 0]),
        np.array([1, 1])
    ))
    assert abs(findBeta(
        np.array([0, 5]),
        np.array([5, 5]),
        np.array([5, 0]),
        np.array([0, 0]),
        np.array([1, 1])
    ) - 0.8) < 0.001
